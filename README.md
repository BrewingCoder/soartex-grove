## Credits

Thanks to Shoeboxam for permission to make fork and his team for beautiful work.

Forked from [Invictus](https://github.com/InvictusGraphics/Invictus_Textures) made by Shoeboxam.

Rest and Peace original [Soar49's 169 public domain Soartex textures](http://www.minecraftforum.net/topic/150915-).
